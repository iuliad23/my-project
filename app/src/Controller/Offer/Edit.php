<?php

declare(strict_types=1);

namespace App\Controller\Offer;

use App\Entity\Offer;
use App\Offer\OfferDtoFactory;
use App\Repository\InsuredPersonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

final class Edit extends AbstractController
{
    #[Route('/api/offers/{id}/update-insured-person', name: 'update_insured_person', methods: ['POST'])]
    public function updateInsuredPerson(
        Request                 $request,
        int                     $id,
        OfferDtoFactory         $offerFactory,
        InsuredPersonRepository $insuredPersonRepository,
        EntityManagerInterface  $entityManager
    ): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $existingOffer = $entityManager->getRepository(Offer::class)->find($id);

        if (!$existingOffer) {
            throw new NotFoundHttpException('Offer not found');
        }

        if ($existingOffer->getCreatedBy() !== $this->getUser()) {
            return new JsonResponse(['message' => 'You are not allowed to update this offer'], Response::HTTP_FORBIDDEN);
        }
        $newInsuredPersonId = $data['insuredPersonId'] ?? null;
        if (!$newInsuredPersonId) {
            return new JsonResponse(['message' => 'New insured person ID is required'], Response::HTTP_BAD_REQUEST);
        }

        $newInsuredPerson = $insuredPersonRepository->find($newInsuredPersonId);

        if (!$newInsuredPerson) {
            return new JsonResponse(['message' => 'New insured person not found'], Response::HTTP_NOT_FOUND);
        }

        try {
            $updatedOffer = $offerFactory->updateInsuredPerson($existingOffer, $newInsuredPerson);

            $entityManager->persist($updatedOffer);
            $entityManager->flush();

            return new JsonResponse(['message' => 'Insured person updated successfully'], Response::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => 'An error occurred while updating insured person'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
