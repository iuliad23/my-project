<?php

declare(strict_types=1);

namespace App\Offer;

use App\Entity\InsuredPerson;
use App\Entity\Offer;
use App\Entity\User;
use App\Repository\CoverageTypeRepository;
use App\Repository\InsuredPersonRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Security;

class OfferDtoFactory
{
    private InsuredPersonRepository $insuredPersonRepository;
    private CoverageTypeRepository $coverageTypeRepository;
    private Security $security;

    public function __construct(
        InsuredPersonRepository $insuredPersonRepository,
        CoverageTypeRepository $coverageTypeRepository,
        Security $security
    ) {
        $this->insuredPersonRepository = $insuredPersonRepository;
        $this->coverageTypeRepository = $coverageTypeRepository;
        $this->security = $security;
    }

    public function createFromDto(OfferDto $offerDto): Offer
    {
        $insuredPerson = $this->insuredPersonRepository->find($offerDto->insuredPersonId);

        if (!$insuredPerson) {
            throw new NotFoundHttpException('Insured person not found');
        }

        /** @var User $createdBy */
        $createdBy = $this->security->getUser();

        $offer = new Offer(
            $offerDto->offerNumber,
            $offerDto->creationDate,
            $offerDto->premium,
            $offerDto->isCanceled,
            $insuredPerson,
            $createdBy,
            $offerDto->description,
            $offerDto->termMonths,
            $offerDto->expirationDate
        );

        foreach ($offerDto->coverageTypeIds as $coverageTypeId) {
            $coverageType = $this->coverageTypeRepository->find($coverageTypeId);

            if (!$coverageType) {
                throw new NotFoundHttpException('Coverage type not found');
            }

            $offer->addCoverageType($coverageType);
        }


        return $offer;
    }
    public function updateInsuredPerson(Offer $offer, InsuredPerson $newInsuredPerson): Offer
    {
        $offer->setInsuredPerson($newInsuredPerson);

        return $offer;
    }
}
