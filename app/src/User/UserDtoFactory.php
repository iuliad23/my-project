<?php

declare(strict_types=1);

namespace App\User;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserDtoFactory
{
    private UserPasswordHasherInterface $passwordHasher;
    private EntityManagerInterface $em;

    public function __construct(UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $em)
    {
        $this->passwordHasher = $passwordHasher;
        $this->em = $em;
    }

    public function createUserFromDto(UserDto $userDto): User
    {
        $existingUser = $this->em->getRepository(User::class)->findOneBy(['email' => $userDto->email]);

        if ($existingUser) {
            throw new \Exception('User already exists');
        }

        $user = new User($userDto->email, $userDto->firstName, $userDto->lastName);

        $user->setPassword($this->passwordHasher->hashPassword($user, $userDto->password));
        $user->setRoles(['ROLE_USER']);

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }
}
