<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\User\UserDtoFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use App\User\UserDto;

final class Register extends AbstractController
{
    #[Route('/api/register', name: 'app_register', methods: ['POST'])]
    public function register(
        Request             $request,
        UserDtoFactory      $userFactory,
        SerializerInterface $serializer
    ): JsonResponse {
        try {
            $userDto = $serializer->deserialize($request->getContent(), UserDto::class, 'json');
        } catch (\Exception $e) {
            return new JsonResponse(['message' => 'Error deserializing UserDto: ' . $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        $userFactory->createUserFromDto($userDto);

        return new JsonResponse(['message' => 'User created successfully'], Response::HTTP_CREATED);
    }
}
