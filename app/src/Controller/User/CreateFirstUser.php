<?php

declare(strict_types=1);

namespace App\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\User\UserDtoFactory;
use App\User\UserDto;

final class CreateFirstUser extends AbstractController
{
    #[Route('/create-first-user', name: 'create_first_user', methods: ['POST'])]
    public function createFirstUser(
        UserDtoFactory $userFactory
    ): JsonResponse {
        $userDto = new UserDto();
        $userDto->email = 'firstuser@test.com';
        $userDto->firstName = 'First';
        $userDto->lastName = 'User';
        $userDto->password = 'secret';

        try {
            $userFactory->createUserFromDto($userDto);

            return new JsonResponse(['message' => 'First user created successfully'], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => 'Error creating user'], Response::HTTP_BAD_REQUEST);
        }
    }
}
