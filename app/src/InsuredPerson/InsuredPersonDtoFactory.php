<?php

declare(strict_types=1);

namespace App\InsuredPerson;

use App\Entity\InsuredPerson;

class InsuredPersonDtoFactory
{
    public function createInsuredPersonFromDto(InsuredPersonDto $insuredPersonDto): InsuredPerson
    {
        return new InsuredPerson(
            $insuredPersonDto->firstName,
            $insuredPersonDto->lastName,
            $insuredPersonDto->cnp
        );
    }
}
