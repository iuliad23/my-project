<?php

declare(strict_types=1);

namespace App\Controller\CoverageType;

use App\CoverageType\CoverageTypeDto;
use App\CoverageType\CoverageTypeDtoFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class Create extends AbstractController
{
    #[Route('/api/coverage-types', name: 'api_create_coverage_type', methods: ['POST'])]
    public function createCoverageType(
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        CoverageTypeDtoFactory $coverageTypeDtoFactory,
        ValidatorInterface $validator
    ): JsonResponse {
        $data = $request->getContent();
        $coverageTypeDto = $serializer->deserialize($data, CoverageTypeDto::class, 'json');

        $errors = $validator->validate($coverageTypeDto);

        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return new JsonResponse(['errors' => $errorMessages], Response::HTTP_BAD_REQUEST);
        }

        $coverageType = $coverageTypeDtoFactory->createCoverageTypeFromDto($coverageTypeDto);

        $entityManager->persist($coverageType);
        $entityManager->flush();

        return new JsonResponse(['message' => 'Coverage type created successfully'], Response::HTTP_CREATED);
    }
}
