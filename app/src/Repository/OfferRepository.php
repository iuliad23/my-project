<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Offer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;


class OfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Offer::class);
    }

    public function findOffersCreatedBy(?int $userId)
    {
        if (!$userId) {
            return [];
        }

        return $this->createQueryBuilder('o')
            ->select('o.id, o.offerNumber, o.creationDate, o.premium')
            ->leftJoin('o.coverageTypes', 'ct')
            ->addSelect('ct.id AS coverageTypeId, ct.name AS coverageTypeName')
            ->where('o.createdBy = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getArrayResult();
    }


    /**
     * @throws NonUniqueResultException
     */
    public function findOfferById(int $id): ?Offer
    {
        return $this->createQueryBuilder('o')
            ->where('o.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
