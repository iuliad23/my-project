# Insurance Product Management Application

Welcome to the Insurance Product Management Application! This application is designed to manage various aspects of insurance products. It offers a range of functionalities to handle user registration, authentication, insurance person creation, coverage type creation for offers, offer editing, viewing user-specific offers, insurance person editing within offers, and offer cancellation.
## Features


- User Registration and Authentication: Users can register an account and log in using JWT (JSON Web Tokens) for secure authentication.

- Insurance Person Creation: Create insurance persons with essential details such as first name, last name, and CNP (Unique Personal Identification Code).

- Coverage Type Creation: Define coverage types for offers, specifying their name, description, and type.

- Insurance Person Editing within Offers: Modify insurance person details within an offer, providing flexibility in managing policies.

- View User-Specific Offers: Users can view and manage their own offers, ensuring a personalized and secure experience.

- Offer Cancellation: Users can cancel existing offers, ensuring efficient policy management.

## Getting Started

To start using the Insurance Product Management Application:
### Prerequisites

Before running the application, ensure you have the following installed:

- Docker and Docker Compose

### Installation

1. Clone this repository to your local machine:
```bash
git clone https://gitlab.com/iuliad23/my-project.git
```
2. Navigate to the project directory:
  ```bash 
cd my-project 
```
3. Build and start the Docker containers:
  ```bash 
docker-compose up -d
```
4. Install dependencies and set up the Symfony application
  ```bash 
docker-compose exec php composer install
```

### Usage

To access the Symfony application, open your web browser and navigate to `http://localhost:8080`.

###
