<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\CoverageTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CoverageTypeRepository::class)]
#[UniqueEntity('name')]
class CoverageType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Assert\NotBlank]
    #[Assert\NotNull]
    protected ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    protected ?string $description = null;

    #[ORM\Column(type: Types::STRING, nullable: true)]
    protected ?string $type = null;

    #[ORM\ManyToMany(targetEntity: Offer::class, mappedBy: 'coverageTypes')]
    protected ?Collection $offers = null;

    public function __construct(
        ?string $name,
        ?string $description = null,
        ?string $type = null
    ) {
        $this->name = $name;
        $this->description = $description;
        $this->type = $type;
        $this->offers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function getOffers(): ?Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): void
    {
            $this->offers->add($offer);
            $offer->addCoverageType($this);
    }
}
