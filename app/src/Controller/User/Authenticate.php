<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Repository\UserRepository;
use App\User\UserDto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

final class Authenticate extends AbstractController
{
    #[Route('/api/login', name: 'app_login', methods: ['POST'])]
    public function login(
        Request $request,
        UserPasswordHasherInterface $passwordHasher,
        JWTTokenManagerInterface $jwtManager,
        SerializerInterface $serializer,
        UserRepository $userRepository
    ): JsonResponse {
        try {
            /** @var UserDto $userDto */
            $userDto = $serializer->deserialize($request->getContent(), UserDto::class, 'json');
        } catch (NotEncodableValueException $e) {
            return new JsonResponse(['message' => 'Invalid JSON data'], Response::HTTP_BAD_REQUEST);
        }

        $user = $userRepository->findOneBy(['email' => $userDto->email]);

        if (!$user || !$passwordHasher->isPasswordValid($user, $userDto->password)) {
            throw new CustomUserMessageAuthenticationException('Invalid credentials');
        }

        $userData = [
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName(),
            'password' => $user->getPassword(),
            'roles' => $user->getRoles(),
        ];

        $token = $jwtManager->create($user);

        return new JsonResponse([
            'token' => $token,
            'user' => $userData,
        ], Response::HTTP_OK);
    }
}
