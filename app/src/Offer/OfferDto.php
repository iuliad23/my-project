<?php

declare(strict_types=1);

namespace App\Offer;

use DateTimeInterface;
use Symfony\Component\Validator\Constraints as Assert;

class OfferDto
{
    #[Assert\NotBlank]
    #[Assert\NotNull]
    public ?string $offerNumber = null;

    public ?DateTimeInterface $creationDate = null;

    public ?string $premium = null;

    public ?bool $isCanceled = null;

    public ?int $insuredPersonId = null;

    public ?string $description = null;

    public ?DateTimeInterface $termMonths = null;

    public ?DateTimeInterface $expirationDate = null;

    public ?array $coverageTypeIds = null;
}
