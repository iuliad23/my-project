<?php

declare(strict_types=1);

namespace App\InsuredPerson;

use Symfony\Component\Validator\Constraints as Assert;

class InsuredPersonDto
{
    #[Assert\NotBlank]
    #[Assert\NotNull]
    public ?string $firstName = null;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    public ?string $lastName = null;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    public ?string $cnp = null;
}
