<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230809054634 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE offer (id INT AUTO_INCREMENT NOT NULL, offer_number VARCHAR(255) NOT NULL, creation_date DATETIME NOT NULL, premium NUMERIC(10, 2) NOT NULL, is_canceled TINYINT(1) NOT NULL, description LONGTEXT DEFAULT NULL, term_months DATETIME DEFAULT NULL, expiration_date DATETIME DEFAULT NULL, INDEX IDX_29D6873EB03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE offer_coverage_type (offer_id INT NOT NULL, coverage_type_id INT NOT NULL, INDEX IDX_5863703F53C674EE (offer_id), INDEX IDX_5863703F43375A16 (coverage_type_id), PRIMARY KEY(offer_id, coverage_type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE offer_coverage_type ADD CONSTRAINT FK_5863703F53C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE offer_coverage_type ADD CONSTRAINT FK_5863703F43375A16 FOREIGN KEY (coverage_type_id) REFERENCES coverage_type (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_29D6873EB03A8386');
        $this->addSql('ALTER TABLE offer_coverage_type DROP FOREIGN KEY FK_5863703F53C674EE');
        $this->addSql('ALTER TABLE offer_coverage_type DROP FOREIGN KEY FK_5863703F43375A16');
        $this->addSql('DROP TABLE offer');
        $this->addSql('DROP TABLE offer_coverage_type');
    }
}
