<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\InsuredPersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: InsuredPersonRepository::class)]
#[UniqueEntity('cnp')]
class InsuredPerson
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected ?int $id = null;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[ORM\Column(length: 255)]
    protected ?string $firstName = null;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[ORM\Column(length: 255)]
    protected ?string $lastName = null;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[ORM\Column(length: 13, unique: true)]
    protected ?string $cnp = null;

    #[ORM\OneToMany(mappedBy: 'insuredPerson', targetEntity: Offer::class)]
    protected ?Collection $offers = null;

    public function __construct(string $firstName, string $lastName, string $cnp)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->cnp = $cnp;
        $this->offers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getCnp(): ?string
    {
        return $this->cnp;
    }

    public function getOffers(): ?Collection
    {
        return $this->offers;
    }
}
