<?php

declare(strict_types=1);

namespace App\CoverageType;

use Symfony\Component\Validator\Constraints as Assert;

class CoverageTypeDto
{
    #[Assert\NotBlank]
    #[Assert\NotNull]
    public ?string $name = null;

    public ?string $description = null;

    public ?string $type = null;
}
