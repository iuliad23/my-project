<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230811111212 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE UNIQUE INDEX UNIQ_43723345E237E06 ON coverage_type (name)');
        $this->addSql('DROP INDEX UNIQ_3BEEA4711EAB9B7E ON insured_person');
        $this->addSql('ALTER TABLE offer ADD insured_person_id INT NOT NULL, ADD created_by_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873EBEAB71FE FOREIGN KEY (insured_person_id) REFERENCES insured_person (id)');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873EB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_29D6873EBEAB71FE ON offer (insured_person_id)');
        $this->addSql('CREATE INDEX IDX_29D6873EB03A8386 ON offer (created_by_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX UNIQ_43723345E237E06 ON coverage_type');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3BEEA4711EAB9B7E ON insured_person (cnp)');
        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_29D6873EBEAB71FE');
        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_29D6873EB03A8386');
        $this->addSql('DROP INDEX IDX_29D6873EBEAB71FE ON offer');
        $this->addSql('DROP INDEX IDX_29D6873EB03A8386 ON offer');
        $this->addSql('ALTER TABLE offer DROP insured_person_id, DROP created_by_id');
    }
}
