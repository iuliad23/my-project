<?php

declare(strict_types=1);

namespace App\User;

class UserDto
{
    public ?string $email = null;

    public ?string $firstName = null;

    public ?string $lastName = null;

    public ?string $password = null;
}
