<?php

declare(strict_types=1);

namespace App\CoverageType;

use App\Entity\CoverageType;

class CoverageTypeDtoFactory
{
    public function createCoverageTypeFromDto(CoverageTypeDto $coverageTypeDto): CoverageType
    {
        return  new CoverageType(
            $coverageTypeDto->name,
            $coverageTypeDto->description,
            $coverageTypeDto->type
        );
    }
}
