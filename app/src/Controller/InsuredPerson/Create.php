<?php

declare(strict_types=1);

namespace App\Controller\InsuredPerson;

use App\InsuredPerson\InsuredPersonDto;
use App\InsuredPerson\InsuredPersonDtoFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class Create extends AbstractController
{
    #[Route('/api/insured-persons', name: 'api_create_insured_person', methods: ['POST'])]
    public function createInsuredPerson(
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        InsuredPersonDtoFactory $insuredPersonDtoFactory,
        ValidatorInterface $validator
    ): JsonResponse {
        $data = $request->getContent();
        $insuredPersonDto = $serializer->deserialize($data, InsuredPersonDto::class, 'json');

        $errors = $validator->validate($insuredPersonDto);

        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return new JsonResponse(['errors' => $errorMessages], Response::HTTP_BAD_REQUEST);
        }

        $insuredPerson = $insuredPersonDtoFactory->createInsuredPersonFromDto($insuredPersonDto);

        $entityManager->persist($insuredPerson);
        $entityManager->flush();

        return new JsonResponse(['message' => 'Insured Person created successfully'], Response::HTTP_CREATED);
    }
}
