<?php

declare(strict_types=1);

namespace App\Controller\Offer;

use App\Entity\Offer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class Cancel extends AbstractController
{
    #[Route('/api/offers/{id}/cancel', name: 'cancel_offer', methods: ['POST'])]
    public function cancelOffer(int $id, EntityManagerInterface $entityManager): JsonResponse
    {
        $offer = $entityManager->getRepository(Offer::class)->find($id);

        if (!$offer) {
            throw new NotFoundHttpException('Offer not found');
        }

        if ($offer->getCreatedBy() !== $this->getUser()) {
            throw new AccessDeniedHttpException('You are not allowed to cancel this offer');
        }

        try {
            $offer->cancelOffer();
            $entityManager->flush();
            return new JsonResponse(['message' => 'Offer canceled successfully'], Response::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => 'An error occurred while canceling the offer'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
