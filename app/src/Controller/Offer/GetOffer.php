<?php

declare(strict_types=1);

namespace App\Controller\Offer;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OfferRepository;

class GetOffer extends AbstractController
{

    #[Route('/api/list-offers', name: 'app_list_offers', methods: ['GET'])]
    public function listOffers(OfferRepository $offerRepository): JsonResponse
    {
        $currentUser = $this->getUser();
        $currentUserId = $currentUser->getId();

        $offers = $offerRepository->findOffersCreatedBy($currentUserId);

        return $this->json($offers);
    }
}
