<?php

declare(strict_types=1);

namespace App\Controller\Offer;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OfferRepository;

class ViewOffer extends AbstractController
{
    #[Route('/api/view-offer/{id}', name: 'app_view_offer', requirements: ['id' => '\d+'], methods: ['GET'])]
    public function viewOfferDetails($id, OfferRepository $offerRepository): JsonResponse
    {
        $id = intval($id);
        $offer = $offerRepository->findOfferById($id);

        if (!$offer) {
            return $this->json(['message' => 'Offer not found'], Response::HTTP_NOT_FOUND);
        }

        if ($offer->getCreatedBy() !== $this->getUser()) {
            return $this->json(['message' => 'You are not allowed to view this offer'], Response::HTTP_FORBIDDEN);
        }

        return $this->json($offer);
    }
}
