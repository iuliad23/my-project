<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\OfferRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: OfferRepository::class)]
#[UniqueEntity('offerNumber')]
class Offer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Assert\NotNull]
    protected ?string $offerNumber = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    protected ?\DateTimeInterface $creationDate = null;

    #[ORM\ManyToMany(targetEntity: CoverageType::class, inversedBy: 'offers')]
    #[ORM\JoinTable(name: 'offer_coverage_type')]
    protected ?Collection $coverageTypes = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    protected ?string $premium = null;

    #[ORM\Column]
    protected ?bool $isCanceled = null;

    #[ORM\ManyToOne( targetEntity: InsuredPerson::class, inversedBy: 'offers')]
   #[ORM\JoinColumn(nullable: false)]
    protected ?InsuredPerson $insuredPerson = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    protected ?string $description = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    protected ?\DateTimeInterface $termMonths = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    protected ?\DateTimeInterface $expirationDate = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name:"created_by_id", referencedColumnName:"id")]
    protected ?User $createdBy;

    public function __construct(
        string              $offerNumber,
        \DateTimeInterface  $creationDate,
        string              $premium,
        bool                $isCanceled,
        InsuredPerson       $insuredPerson,
        User                 $createdBy,
        ?string             $description = null,
        ?\DateTimeInterface $termMonths = null,
        ?\DateTimeInterface $expirationDate = null
    )
    {
        $this->offerNumber = $offerNumber;
        $this->creationDate = $creationDate;
        $this->premium = $premium;
        $this->isCanceled = $isCanceled;
        $this->insuredPerson = $insuredPerson;
        $this->createdBy = $createdBy;
        $this->description = $description;
        $this->termMonths = $termMonths;
        $this->expirationDate = $expirationDate;
        $this->coverageTypes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOfferNumber(): ?string
    {
        return $this->offerNumber;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function getPremium(): ?string
    {
        return $this->premium;
    }

    public function isIsCanceled(): ?bool
    {
        return $this->isCanceled;
    }

    public function getInsuredPerson(): ?InsuredPerson
    {
        return $this->insuredPerson;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getTermMonths(): ?\DateTimeInterface
    {
        return $this->termMonths;
    }

    public function getExpirationDate(): ?\DateTimeInterface
    {
        return $this->expirationDate;
    }

    public function getCoverageTypes(): ?Collection
    {
        return $this->coverageTypes;
    }

    public function addCoverageType(CoverageType $coverageType): void
    {
        if (!$this->coverageTypes->contains($coverageType)) {
            $this->coverageTypes->add($coverageType);

            $coverageType->addOffer($this);
        }
    }

    public function setInsuredPerson(InsuredPerson $insuredPerson): void
    {
        $this->insuredPerson = $insuredPerson;
    }

    public function cancelOffer(): void
    {
        $this->isCanceled = true;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }
}
