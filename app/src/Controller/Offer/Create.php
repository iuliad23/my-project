<?php

declare(strict_types=1);

namespace App\Controller\Offer;

use App\Offer\OfferDto;
use App\Offer\OfferDtoFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class Create extends AbstractController
{
    #[Route('/api/create-offer', name: 'app_create_offer', methods: ['POST'])]
    public function createOffer(
        Request $request,
        SerializerInterface $serializer,
        OfferDtoFactory $offerDtoFactory,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator
    ): JsonResponse {
        $data = $request->getContent();
        $offerDto = $serializer->deserialize($data, OfferDto::class, 'json');

        $errors = $validator->validate($offerDto);

        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return new JsonResponse(['errors' => $errorMessages], Response::HTTP_BAD_REQUEST);
        }

        $offer = $offerDtoFactory->createFromDto($offerDto);

        $entityManager->persist($offer);
        $entityManager->flush();

        return new JsonResponse(['message' => 'Offer created successfully'], Response::HTTP_CREATED);
    }
}
